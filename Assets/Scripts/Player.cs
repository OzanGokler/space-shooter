﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    Ship ship;
    Rigidbody rigidbody;
    public Text speedText;
    public GameObject player;
    float givePower;
    float giveRotationHorizontalPower;
    float giveTargetHorizontal;
    float giveTargetVertical;

    void Awake()
    {
        GameStatic.player = this;
        rigidbody = GetComponent<Rigidbody>();
    }
    void Start()
    {
        ship = GameStatic.def.defaultShip;
        StartCoroutine(move());
    }
    void Update()
    {
        //current speed text in screen
        //speedText.text = "Speed: " + rigidbody.velocity;     
        Controller();
    }
    void Controller()
    {
        givePower = Input.GetAxis("LeftAnalogVertical");
        giveRotationHorizontalPower = Input.GetAxis("LeftAnalogHorizontal");
        giveTargetHorizontal = Input.GetAxis("RightAnalogHorizontal");
        giveTargetVertical = Input.GetAxis("RightAnalogVertical");
    }

    IEnumerator move()
    {
        while (true)
        {         
            rigidbody.velocity = transform.forward * ship.GetThursterForwardPower(givePower);
            rigidbody.transform.Rotate(new Vector3(-ship.GetVerticalTargetPower(giveTargetVertical), ship.GetHorizontalTargetPower(giveTargetHorizontal), -ship.GetHorizontalRotationPower(giveRotationHorizontalPower)));

            yield return new WaitForSeconds(1/60);
        }
    }
    public void spawnShip(string shipPrefabName)
    {
        Debug.Log("asklşdj");
        GameObject prefab = Instantiate(Resources.Load(shipPrefabName)) as GameObject;
        prefab.transform.position = player.transform.position;
        prefab.transform.parent = player.transform;
    }
}
