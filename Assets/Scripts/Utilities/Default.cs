﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Default : MonoBehaviour {

    public Ship defaultShip = new Ship();

    public float maxPower;
    public float minPower;
    public float accPeriod;
    public float speedDownPeriod;
    public float breakDownPeriod;
    public float horizontalRotationPower;
    public float horizontalTargetPower;
    public float verticalTargetPower;
    //public float horiPower;
    //public float veriPower;

    void Awake () {
        GameStatic.def = this;

        Thruster thruster = new Thruster();
        thruster.accelerationPeriod = accPeriod;
        thruster.maxFowardPower = maxPower;
        thruster.minFowardPower = minPower;
        thruster.speedDownPeriod = speedDownPeriod;
        thruster.breakDownPeriod = breakDownPeriod;
        thruster.horizontalRotationPower = horizontalRotationPower;
        thruster.horizontalTargetPower = horizontalTargetPower;
        thruster.verticalTargetPower = verticalTargetPower;
        defaultShip.thruster = thruster;
	}
	
	void Update () {
		
	}
}
