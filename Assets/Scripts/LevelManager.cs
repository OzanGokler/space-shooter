﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityScript;

public class LevelManager : MonoBehaviour {
    public Transform tr_spawnPoint;
    public Canvas CanvasBeginMenu;
    public Camera MainCamera;
    public string shipPrefabName;
    private void Awake()
    {
        
    }
    void Start () {
        
    }

    public void clickPracticeButton()
    {
        createPlayer(tr_spawnPoint);
        MainCamera.enabled = false;
        Debug.Log("ahanda");
        CanvasBeginMenu.enabled = false;
        spawnShip(shipPrefabName);
    }

    public void createPlayer(Transform spawnPoint)
    {
        Instantiate(Resources.Load("Player"), spawnPoint.position, spawnPoint.rotation);

    }
    public void spawnShip(string shipPrefabName)
    {       
        GameStatic.player.spawnShip(shipPrefabName);
    }
	void Update () {
		
	}
}
