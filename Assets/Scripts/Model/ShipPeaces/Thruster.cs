﻿using System;
using UnityEngine;

public class Thruster{
    public float forwardPower;
    public float maxFowardPower;
    public float minFowardPower;
    public float accelerationPeriod;
    public float speedDownPeriod;
    public float breakDownPeriod;

    public float horizontalRotationPower;
    public float horizontalTargetPower;
    public float verticalTargetPower;

    float timePower;

    public float GetThursterForwardPower(float power)
    {
        if (accelerationPeriod < 1)
            accelerationPeriod = 1;
        if (breakDownPeriod < 1)
            breakDownPeriod = 1;

        if(power > 0)
        {
            //SPEED UP
            if ((timePower + accelerationPeriod - power < Time.time))
            {
                if (forwardPower <= maxFowardPower)
                {
                    forwardPower++;
                }
                timePower = Time.time;
            }
        }
        else if(power == 0)
        {
            //SPEED DOWN FOR MOMENT
            if ((timePower + speedDownPeriod < Time.time))
            {
                if (forwardPower > minFowardPower)
                {
                    forwardPower--;
                }
                timePower = Time.time;
            }
        }
        else
        {
            //SPEEDDOWN
            if ((timePower + breakDownPeriod + power < Time.time))
            {
                if (forwardPower > minFowardPower)
                {
                    forwardPower--;
                }
                timePower = Time.time;
            }
        }      
        return forwardPower;
    }
    public float GetHorizontalRotationPower(float power)
    {
        if (power > 0)
        {
            //rotate Left
            return horizontalRotationPower * power;             
        }
        else if(power < 0)
        {
            //rotate Right         
            return horizontalRotationPower * power;        
        }
        else
        {
            //not rotate
            return 0;
        }
    }
    public float GetHorizontalTargetPower(float power)
    {
        if (power > 0)
        {
            return horizontalTargetPower * power;
        }
        else if (power < 0)
        {      
            return horizontalTargetPower * power;
        }
        else
        {
            return 0;
        }
    }
    public float GetVerticalTargetPower(float power)
    {
        if (power > 0)
        {
            return verticalTargetPower * power;
        }
        else if (power < 0)
        {
            return verticalTargetPower * power;
        }
        else
        {
            return 0;
        }
    }

}
