﻿using UnityEngine;

public class Ship {

    public Hull hull;
    public Scanner scanner;
    public Weapon[] weapons;
    public Shield[] shields;
    public Thruster thruster;

    public float GetThursterForwardPower(float power)
    {
        return thruster.GetThursterForwardPower(power);
    }
    public float GetHorizontalRotationPower(float power)
    {
        return thruster.GetHorizontalRotationPower(power);
    }
    public float GetHorizontalTargetPower(float power)
    {
        return thruster.GetHorizontalTargetPower(power);
    }

    public float GetVerticalTargetPower(float power)
    {
        return thruster.GetVerticalTargetPower(power);
    }
}
